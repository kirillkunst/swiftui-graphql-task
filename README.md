# SwiftUI + GraphQL test task

## Architecture

1. I chose simple Redux implementation to have an app state in one place and have Unidirectional data flow.
2. ViewModels to configure UI base on model properties.
3. Service and Redux Store are injected as environment objects to simplify the logic.
4. You can find typealiases for GraphQL struct to make the code clearier.

## Project Structure

- App

    Here are we have our Scene and App delegates. All initial configuration for Redux store, service to get the data is located in `SceneDelegate`.
- Extensions
    
    Some useful extension like getting Color from hex string

- Constants

    Here I placed constants for Server url and Colors.
    
- GraphQL

    All GraphQL stuff. Contains `schema.json` and `widgets.graphql` file with the query to the server.

- Redux
    - Actions
    - State
    - Reducers
    - Store

    I placed all Redux-related code here in this folder.

- Resouces
    
    Assets and launch screen storyboard.

- Service
    - TransactionService.swift
        
        Here I created a Combine wrapper to get a data from GraphQL API.swift
    
    - Formatter.swift

        Here I placed all functions to format date and currency.

- ViewModels
    
    View models for `DaySection`, `Transaction` and `Feed` data.

- Views

    SwiftUI-related code.
    
    I created a wrapper for `UIActivityIndicatorView` to show a progress while we're waiting for data from the server.

    `TransactionsListContainerView` - container for the entire screen app, contains Redux `store` and `actions` to fetch the data.

    `TransactionsList` - configured list of transactions.

    `DaySectionWidgetView` - view for DaySectionWidget item.

    `TransactionWidgetView` - view for TransactionWidget item. 


## Dependencies

This project uses Apollo iOS library to work with GraphQL. It's connected to the project via Swift Package Manager.