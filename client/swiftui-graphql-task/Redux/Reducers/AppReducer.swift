//
//  AppReducer.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 31.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation
import Combine

struct Reducers {
    static func appReducer(state: inout AppState, action: AppAction) {
        switch action {
        case let .setTransactions(transactions):
            state.transactions = transactions
        case .fetchTransactions:
            break
        }
    }
}


