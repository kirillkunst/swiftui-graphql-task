//
//  AppAction.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 31.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation
import Combine

enum AppAction {
    case fetchTransactions
    case setTransactions(transactions: [Feed])
}

class TransactionActions: ObservableObject {
    
    let service: TransactionService
    
    init(service: TransactionService) {
        self.service = service
    }
    
    func fetch() -> AnyPublisher<AppAction, Never> {
        self.service.fetch().replaceError(with: [])
            .map { AppAction.setTransactions(transactions: $0) }
            .eraseToAnyPublisher()
    }
}


