//
//  Store.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 30.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation
import Combine

typealias Reducer<State, Action> = (inout State, Action) -> Void

final class Store<State, Action>: ObservableObject {
    typealias Effect = AnyPublisher<Action, Never>
    
    @Published private(set) var state: State

    private let reducer: Reducer<State, Action>
    private var cancellables: Set<AnyCancellable> = []

    init(initialState: State, reducer: @escaping Reducer<State, Action>) {
        self.state = initialState
        self.reducer = reducer
    }

    func dispatch(_ action: Action) {
        reducer(&state, action)
    }

    func dispatch(_ effect: Effect) {
        var cancellable: AnyCancellable?
        var didComplete = false

        cancellable = effect
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] _ in
                    didComplete = true
                    if let effectCancellable = cancellable {
                        self?.cancellables.remove(effectCancellable)
                    }
                }, receiveValue: dispatch)

        if !didComplete, let effectCancellable = cancellable {
            cancellables.insert(effectCancellable)
        }
    }
}
