//
//  TransactionService.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 31.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation
import Apollo
import Combine

typealias Feed = FeedQuery.Data.DailyTransactionsFeed
typealias Transaction = FeedQuery.Data.DailyTransactionsFeed.AsTransactionWidget
typealias DaySection = FeedQuery.Data.DailyTransactionsFeed.AsDaySectionWidget

protocol TransactionService {
    func fetch() -> Future<[Feed], Error>
}

class ApolloTransactionService: TransactionService {
    
    private(set) var apollo: ApolloClient
    
    init(url: URL) {
        self.apollo = ApolloClient(url: url)
    }
    
    func fetch() -> Future<[Feed], Error> {
        return Future<[Feed], Error> { promise in
            self.apollo.fetch(query: FeedQuery()) { result in
                switch result {
                case .success(let data):
                    if let data = data.data?.dailyTransactionsFeed {
                        let feed = data.compactMap({ $0 })
                        return promise(.success(feed))
                    }
                case .failure(let error):
                    return promise(.failure(error))
                }
            }
        }
    }
    
    
    
}
