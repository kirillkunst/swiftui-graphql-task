//
//  Formatter.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 31.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation

struct Formatter {
    private static let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.decimalSeparator = "."
        formatter.maximumFractionDigits = 2
        return formatter
    }()
    
    public static func formatDate(_ date: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let date = formatter.date(from: date)
        formatter.dateFormat = ""
        formatter.dateStyle = .medium
        return formatter.string(from: date!)
    }
    
    public static func formatAmount(_ amount: Double, currencyCode: String) -> String {
        self.numberFormatter.locale = locale(from: currencyCode)
        let sign: String = amount > 0 ? self.numberFormatter.plusSign : ""
        let value = self.numberFormatter.string(from: NSNumber(value: amount)) ?? ""
        return "\(sign)\(value)"
    }
    
    private static func locale(from currencyCode: String) -> Locale {
        switch currencyCode {
        case "RUB":
            return Locale(identifier: "ru_RU")
        case "USD":
            return Locale(identifier: "en_US")
        case "GBP":
            return Locale(identifier: "en_GB")
        default:
            return Locale(identifier: "en_US")
        }
    }
}
