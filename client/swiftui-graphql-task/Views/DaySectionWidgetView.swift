//
//  DaySectionWidgetView.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 31.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

struct DaySectionWidgetView: View {
    var section: DaySectionViewModel
    
    var body: some View {
        VStack {
            
            Rectangle()
                .fill(Constants.Colors.lightBlue)
            .frame(height: 10)
            .edgesIgnoringSafeArea(.horizontal)
                .padding(.bottom, 10)
            
            HStack {
                Text(self.section.formattedDate)
                    .foregroundColor(Constants.Colors.lightGray)
                    .padding(.leading, 15)
                
                Spacer()
                
                Text(self.section.formattedAmount)
                    .bold()
                    .frame(maxWidth: .infinity, alignment: .trailing)
                    .padding(.trailing, 15)
                
            }.padding(.horizontal, 15)
            
        }.padding(.vertical, 20)
    }
}
