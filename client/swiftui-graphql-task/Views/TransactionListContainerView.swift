//
//  TransactionListContainerView.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 31.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

struct TransactionsListContainerView: View {
    
    @EnvironmentObject var store: Store<AppState, AppAction>
    @EnvironmentObject var actions: TransactionActions
    
    var body: some View {
        TransactionsList(feed: store.state.transactions.map({ FeedViewModel(item: $0) }))
            .onAppear(perform: fetch)
    }
    
    private func fetch() {
        store.dispatch(actions.fetch())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        TransactionsListContainerView()
    }
}

