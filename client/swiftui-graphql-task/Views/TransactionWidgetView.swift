//
//  TransactionWidgetView.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 31.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

struct TransactionWidgetView: View {
    var transaction: TransactionViewModel
    
    var body: some View {
        ZStack {
            
            self.transaction.backgroundColor
            
            HStack {
                
                Image(self.transaction.iconName)
                .resizable()
                    .frame(width: 45, height: 45, alignment: .center)
                    .cornerRadius(15).padding(.leading, 15)
                
                Text(self.transaction.title)
                    .foregroundColor(self.transaction.textColor).padding(.leading, 10)
                
                Spacer()
                
                Text(self.transaction.formattedAmount)
                    .foregroundColor(self.transaction.textColor).frame(maxWidth: .infinity, alignment: .trailing)
                    .padding(.trailing, 15)
                
            }.padding(.vertical, 15)
            
        }.cornerRadius(15).padding(.horizontal, 15)
    }
}
