//
//  ContentView.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 30.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import SwiftUI

struct TransactionsList: View {
    @State private var shouldAnimate = true
    
    var feed: [FeedViewModel]
    
    init(feed: [FeedViewModel]) {
        self.feed = feed
        
        // Disable default UITableView separator
        UITableView.appearance().tableFooterView = UIView()
        UITableView.appearance().separatorStyle = .none
    }
    
    var body: some View {
        if feed.isEmpty {
            return AnyView(ActivityIndicator(shouldAnimate: self.$shouldAnimate))
        } else {
            return AnyView(List {
                ForEach(self.feed, id: \.id) { feedItem in
                    self.buildView(for: feedItem).listRowInsets(EdgeInsets())
                }
            })
        }
    }
    
    
    // Build a view based on Feed item's type
    func buildView(for feedItem: FeedViewModel) -> AnyView {
        if let section = feedItem.item.asDaySectionWidget {
            return AnyView(DaySectionWidgetView(section: DaySectionViewModel(section: section)))
        } else if let transaction = feedItem.item.asTransactionWidget {
            return AnyView(TransactionWidgetView(transaction: TransactionViewModel(transaction: transaction)))
        }
        return AnyView(TransactionsList(feed: []))
    }
}

