//
//  Constants.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 31.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

struct Constants {
    
    struct Server {
        static let url = "http://localhost:4000/graphql"
    }
    
    struct Colors {
        static let lightGreen = Color(hex: "#EFFAE6")
        static let green = Color(hex: "#7DA15E")
        static let lightBlue = Color(hex: "#F5F9FE")
        static let lightGray = Color(hex: "#99A2AC")
    }
}
