//
//  FeedViewModel.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 31.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation

struct FeedViewModel: Identifiable {
    var item: Feed
    
    public typealias ID = String
    
    public var id: String {
        if let section = item.asDaySectionWidget {
            return section.date
        } else if let transactionData = item.asTransactionWidget {
            return transactionData.transaction.id
        }
        return ""
    }
}
