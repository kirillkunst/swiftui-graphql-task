//
//  DaySectionViewModel.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 31.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation

struct DaySectionViewModel {
    var section: DaySection
    
    public var formattedDate: String {
        return Formatter.formatDate(section.date)
    }
    
    public var formattedAmount: String {
        if let amountValue = Double(section.amount.value) {
            return Formatter.formatAmount(amountValue, currencyCode: section.amount.currencyCode.rawValue)
        }
        return section.amount.value
    }
}
