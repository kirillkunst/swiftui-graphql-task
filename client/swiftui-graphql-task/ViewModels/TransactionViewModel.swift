//
//  TransactionViewModel.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 31.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import Foundation
import SwiftUI

struct TransactionViewModel {
    var transaction: Transaction
    
    public var title: String {
        return self.transaction.transaction.title
    }
    
    public var iconName: String {
        if self.isCashback {
            return "cashback"
        }
        return transaction.image?.iconName ?? ""
    }
    
    public var backgroundColor: Color {
        self.isCashback ? Constants.Colors.lightGreen : Color.clear
    }
    
    public var textColor: Color {
        self.isCashback ? Constants.Colors.green : Color.black
    }
    
    public var isCashback: Bool {
        if let amountValue = Double(transaction.transaction.amount.value), amountValue > 0 {
            return true
        }
        return false
    }
    
    public var formattedAmount: String {
        if let amountValue = Double(transaction.transaction.amount.value) {
            return Formatter.formatAmount(amountValue, currencyCode: transaction.transaction.amount.currencyCode.rawValue)
        }
        return transaction.transaction.amount.value
    }
}
