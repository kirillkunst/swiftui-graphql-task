//
//  SceneDelegate.swift
//  swiftui-graphql-task
//
//  Created by Kirill Kunst on 30.12.2019.
//  Copyright © 2019 Kirill Kunst. All rights reserved.
//

import UIKit
import SwiftUI

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        let transactionsList = TransactionsListContainerView()

        if let windowScene = scene as? UIWindowScene {
            
            // Create a service to fetch transactions
            let transactionService = ApolloTransactionService(url: URL(string: Constants.Server.url)!)
            
            // Create actions with this service
            let actions = TransactionActions(service: transactionService)
            
            // Configure Redux store
            let store = Store<AppState, AppAction>(initialState: AppState(), reducer: Reducers.appReducer)
            
            let window = UIWindow(windowScene: windowScene)
            
            // Apply store and actions to container view
            window.rootViewController = UIHostingController(rootView: transactionsList
                .environmentObject(store)
                .environmentObject(actions))
            self.window = window
            window.makeKeyAndVisible()
        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
    }

    func sceneWillResignActive(_ scene: UIScene) {
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
    }


}

